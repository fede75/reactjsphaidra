







var DCListing = React.createClass({
  loadobjectsFromServer: function() {
    
	
	var objects=[];
	var loader=this;
	$(document).ready(function(){
	  var url = "https://phaidradev.cab.unipd.it/api/object/o:61439/dc";
	  $.getJSON(url, function(response){
		console.log(response.metadata.dc);
		objects=response.metadata.dc;
	    loader.setState({objects: objects});

	  });
	});
	
  },
  loadSavesFromServer: function() {
    
    var saves = [
      {
        "saves": 52,
        "saved": false
      },
      {
        "saves": 123,
        "saved": true
      },
      {
        "saves": 189,
        "saved": false
      }
    ];
    this.setState({saves: saves});
  },
	toggleSave: function(index) {
    
    var saves = this.state.saves;
    
  	if (saves[index].saved) {
      saves[index].saves--;
      saves[index].saved = false;
		}
		else {
  		saves[index].saves++;
      saves[index].saved = true;
		}
    
    
		this.setState({
			saves: saves,
		});
    
   
    return saves[index].saved;

	},
	getInitialState: function(){

		var saves = [];
    var objects = [];
    
		return {
      saves: saves,
      objects: objects
    }
	},
  componentDidMount: function() {
    this.loadobjectsFromServer();
    this.loadSavesFromServer();
   
  },
  render: function() {
   
    var saves = this.state.saves;
    var toggleSave = this.toggleSave;
    
   
    var dcNodes = this.state.objects.map(function(dc, index) {
      
      if (typeof(saves[index]) == "undefined") {
        saves[index] = {saves: 0};
      }
      
      return (
        <DC
          key={index}
          id={index}
          onToggleSave={toggleSave}
          isSaved={saves[index].saved}
          photo={dc.photo}
          ui_value={dc.ui_value}
          numSaves={saves[index].saves}
        >
          {dc.description}
        </DC>
      );
    });
   
    return (
      <div className="dcList">
        {dcNodes}
      </div>
    );
  }
});

var DC = React.createClass({
  toggleSave: function(index){
    
    return this.props.onToggleSave(index);
  },
  render: function() {
    
    return (
      <div className="dc">
        <span className="dcui_value">
          {this.props.ui_value}
        </span>
        <Photo src={this.props.photo}></Photo>
        <span className="dcDescription">
          {this.props.children}
        </span>
        <Saves
          id={this.props.id}
          handleSave={this.toggleSave}
          isSaved={this.props.isSaved}
          numSaves={this.props.numSaves}
        ></Saves>
      </div>
    );
  }
});


var Photo = (props) => {
  return (<div className="dcPhoto">
    <img src={props.src} />
  </div>);
};

var Saves = React.createClass({
  handleSubmit: function(e) {
    
    e.preventDefault();
    
   
    var isSaved = this.props.handleSave(this.props.id);
  },
  render: function() {
    
    var savedText = '';
    var submitText = 'Save';
    if (this.props.isSaved) {
      savedText = 'You have saved this dc.';
      submitText = 'Remove';
    }
    
    
    return (
      <div className="saves">
        <form onSubmit={this.handleSubmit}>
          <input type="submit" value={submitText} />
        </form>
      {this.props.numSaves} saves. {savedText}
      </div>
    );
  }
});
ReactDOM.render(
  <DCListing url="objects.json" savesUrl="saves.json" pollInterval={2000} />,
  document.getElementById('content')
);
